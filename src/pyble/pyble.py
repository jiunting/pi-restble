import pyble_ext
from threading import Thread


class PyBLE():
    def __init__(self):
        print "PyBLE "

    def set_scan_params(self, scan_type, interval, window, own_type, filter):
        pyble_ext.c_set_scan_params(scan_type, interval, window, own_type, filter)
        
    def scan_start(self):
        pyble_ext.c_scan_start()
        
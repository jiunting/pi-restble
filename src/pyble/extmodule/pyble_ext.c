#include <Python.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <unistd.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

 
/**  int hci_le_set_scan_parameters(int dev_id, uint8_t type, uint16_t interval,
 *       uint16_t window, uint8_t own_type, uint8_t filter, int to); 
 **/
static PyObject*
c_set_scan_params(PyObject* self, PyObject* args)
{
	const char* name;
    int err, opt, dd;
    uint8_t own_type = 0x00;
    uint8_t scan_type = 0x01;
	uint8_t filter_type = 0;
	uint8_t filter_policy = 0x00;
	uint16_t interval = htobs(0x0010);
	uint16_t window = htobs(0x0010);
	uint8_t filter_dup = 1;
    int dev_id;
    
    printf("default \
            scan_type:%d interval:%d window:%d own_type:%d filter_policy:%d  \n",
                scan_type, interval, window, own_type, filter_policy);
    
    if (!PyArg_ParseTuple(args, "bHHbb", &scan_type, &interval, &window, 
                            &own_type, &filter_policy)) {
         return NULL;                    
    }
 
    printf("set_scan_params \
            scan_type:%d interval:%d window:%d own_type:%d filter_policy:%d  \n",
                scan_type, interval, window, own_type, filter_policy);
    
    /* passing NULL to hci_get_route will retrieve the resource number of 
     * the first available Bluetooth adapter.
    */
    dev_id = hci_get_route(NULL);

    dd = hci_open_dev(dev_id);
	if (dd < 0) {
		printf("Could not open device");
	}

	err = hci_le_set_scan_parameters(dd, scan_type, interval, window,
						own_type, filter_policy, 10000);
	if (err < 0) {
		printf("Set scan parameters failed \n");
	}
    
	hci_close_dev(dd);
    
	Py_RETURN_NONE;
}


static PyObject*
c_hci_read(PyObject* self, PyObject* args)
{
    const char* name;
    unsigned char buf[HCI_MAX_EVENT_SIZE], *ptr;
    struct hci_filter nf, of;
    struct sigaction sa;
	socklen_t olen;
	int len, dev_id, dd;

	if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
 
    /* passing NULL to hci_get_route will retrieve the resource number of 
     * the first available Bluetooth adapter.
    */
    dev_id = hci_get_route(NULL);

    dd = hci_open_dev(dev_id);
    if (dd < 0) {
		printf("Could not open device");
	}

    olen = sizeof(of);
	if (getsockopt(dd, SOL_HCI, HCI_FILTER, &of, &olen) < 0) {
		printf("Could not get socket options\n");
		return -1;
	}
    
    hci_filter_clear(&nf);
    hci_filter_set_ptype(HCI_EVENT_PKT, &nf);
	hci_filter_set_event(EVT_LE_META_EVENT, &nf);

 
	Py_RETURN_NONE;
}

static PyObject*
c_scan_start(PyObject* self, PyObject* args)
{
	const char* name;
    int err, opt, dd, dev_id;
	uint8_t filter_dup = 1;


    /* passing NULL to hci_get_route will retrieve the resource number of 
     * the first available Bluetooth adapter.
    */
    dev_id = hci_get_route(NULL);

    /* open resources to use the devic */
	dd = hci_open_dev(dev_id);
	if (dd < 0) {
		printf("Could not open device");
	}
    
	err = hci_le_set_scan_enable(dd, 0x01, filter_dup, 10000);
	if (err < 0) {
		printf("Enable scan failed \n");
	}

	printf("LE Scan ...\n");
 	
	Py_RETURN_NONE;
}

static PyObject*
c_scan_stop(PyObject* self, PyObject* args)
{
    const char* name;
    int err, opt, dd;
    uint8_t filter_dup = 1;
	
	if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;

    printf("scan_stop %s!\n", name);
    
    err = hci_le_set_scan_enable(dd, 0x00, filter_dup, 10000);
    if (err < 0) {
		printf("Disable scan failed \n");
	}

	hci_close_dev(dd);
 	
	Py_RETURN_NONE;
}

static PyObject*
c_set_adv_data(PyObject* self, PyObject* args)
{
	const char* name;
	
	 if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
     printf("set_adv_data %s!\n", name);
 
	
	Py_RETURN_NONE;
}

static PyObject*
c_set_adv_scan_response_data(PyObject* self, PyObject* args)
{
	const char* name;
	
	 if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;

    printf("set_adv_scan_response_data %s!\n", name);
 
	Py_RETURN_NONE;
}

static PyObject*
c_adv_start(PyObject* self, PyObject* args)
{
	const char* name;
    unsigned char buf[HCI_MAX_EVENT_SIZE], *ptr = buf;
    struct hci_filter flt;
	hci_event_hdr *hdr;
	int i, opt, len, dd, dev_id;
	uint16_t ocf;
	uint8_t ogf;

	if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
 
	printf("adv_start %s!\n", name);
 
	Py_RETURN_NONE;
}

static PyObject*
c_adv_stop(PyObject* self, PyObject* args)
{
    const char* name;
	
	if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
 
	printf("adv_stop %s!\n", name);
 
	Py_RETURN_NONE;
}

static PyMethodDef PyBleMethods[] =
{
    {"c_adv_start", c_adv_start, METH_VARARGS, "Greet somebody."},
    {"c_adv_stop", c_adv_stop, METH_VARARGS, "Greet somebody."},
    {"c_set_adv_scan_response_data", c_set_adv_scan_response_data, METH_VARARGS, "Greet somebody."},
 	{"c_set_adv_data", c_set_adv_data, METH_VARARGS, "Greet somebody."},
	{"c_scan_start", c_scan_start, METH_VARARGS, "Greet somebody."},
    {"c_scan_stop", c_scan_stop, METH_VARARGS, "Greet somebody."},
	{"c_set_scan_params", c_set_scan_params, METH_VARARGS, "Greet somebody."},
    {"c_hci_read", c_hci_read, METH_VARARGS, "Greet somebody."},
	{NULL, NULL, 0, NULL}
};
 
PyMODINIT_FUNC
initpyble_ext(void)
{
    (void) Py_InitModule("pyble_ext", PyBleMethods);
}

from distutils.core import setup, Extension
#from setuptools import find_packages
#from setuptools import setup, Extension, Command 
 
module1 = Extension('pyble_ext', 
                    define_macros = [('MAJOR_VERSION', '1'), ('MINOR_VERSION', '0')],
                    include_dirs = ['/usr/include'],
                    libraries = ['bluetooth'],
                    library_dirs = ['/usr/lib'],
                    sources = ['pyble/extmodule/pyble_ext.c'],
                    )
 
setup (name = 'pyble',
        version = '1.0',
        author = 'Chun-Ting Ding',
        author_email = 'jiunting.d@gmail.com',
        description = 'Bluetooth Low Energy Lib for Python',
        package_dir={'': 'pyble'},
        py_modules=  ['pyble'],
        ext_modules = [module1])

